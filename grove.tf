variable "grove_endpoint" {
    type = string
    default = "http://localhost:3000"
}

provider "aws" {
    region = "us-east-1"
    access_key = "test"
    secret_key = "test"
    s3_use_path_style = true # true: https://s3.amazonaws.com/BUCKET/KEY, false(default): https://BUCKET.s3.amazonaws.com/KEY
    skip_credentials_validation = true
    skip_metadata_api_check = true

    endpoints {
        # https://registry.terraform.io/providers/tfproviders/aws/latest/docs/guides/custom-service-endpoints
        acm             = var.grove_endpoint
        apigateway      = var.grove_endpoint
        cloudwatch      = var.grove_endpoint
        dynamodb        = var.grove_endpoint
        events          = var.grove_endpoint
        iam             = var.grove_endpoint
        kinesis         = var.grove_endpoint
        kms             = var.grove_endpoint
        lambda          = var.grove_endpoint
        logs            = var.grove_endpoint
        s3              = var.grove_endpoint # or "${var.grove_endpoint}/s3" or "http://s3.localhost:3000"?
        secretsmanager  = var.grove_endpoint
        sns             = var.grove_endpoint
        sqs             = var.grove_endpoint
        stepfunctions   = var.grove_endpoint
        sts             = var.grove_endpoint
    }

    default_tags {
    }
}

# resource "aws_s3_bucket" "example" {
#   bucket = "my-tf-test-bucket"
# }

resource "aws_sqs_queue" "sqs_queue" {
    name = "sqs_queue"
    tags = {
        Name = "sqs_queue"
    }
}

locals {
    lambda_path = "./lambdas/"
}
data "aws_iam_policy_document" "lambda_assume_role" {
    statement {
        effect = "Allow"
        principals {
            type        = "Service"
            identifiers = ["lambda.amazonaws.com"]
        }
        actions = ["sts:AssumeRole"]
    }
}
resource "aws_iam_role" "lambda_role" {
    name               = "lambda_role"
    assume_role_policy = data.aws_iam_policy_document.lambda_assume_role.json
}
resource "aws_lambda_function" "hello" {
    s3_bucket        = "local"
    s3_key           = local.lambda_path
    source_code_hash = filebase64sha256("${local.lambda_path}/hello.js")

    function_name    = "hello"
    description      = "Takes in a name and sends a response saying hello to it"
    role             = aws_iam_role.lambda_role.arn
    handler          = "hello.handler"
    runtime          = "nodejs20.x"
}

resource "aws_lambda_event_source_mapping" "sqs_lambda_mapping" {
  event_source_arn = aws_sqs_queue.sqs_queue.arn
  function_name    = aws_lambda_function.hello.function_name
  batch_size       = 10
}

resource "aws_api_gateway_rest_api" "api" {
    name        = "ExampleAPI"
    description = "API for Example Lambda Function"

    body = jsonencode({
        openapi = "3.0.0"
        paths = {
            "/hello" = {
                x-amazon-apigateway-any-method = {
                    x-amazon-apigateway-integration = {
                        httpMethod = "POST"
                        type       = "AWS_PROXY"
                        uri        = aws_lambda_function.hello.invoke_arn
                    }
                }
            },
        }
    })

    tags = {
        "_custom_id_" = "example" # Special tag for a static API ID.
    }
}
resource "aws_api_gateway_deployment" "api" {
    rest_api_id = aws_api_gateway_rest_api.api.id
    stage_name  = "dev"
}
resource "aws_lambda_permission" "api_hello_permission" {
    statement_id  = "AllowExecutionFromAPIGateway"
    action        = "lambda:InvokeFunction"
    function_name = aws_lambda_function.hello.function_name
    principal     = "apigateway.amazonaws.com"
    source_arn = "${aws_api_gateway_rest_api.api.execution_arn}/*/*"
}

resource "aws_dynamodb_table" "game_table" {
    name = "GameScores"
    billing_mode = "PROVISIONED"
    read_capacity = 20
    write_capacity = 20
    hash_key = "UserId" # partition key
    range_key = "GameTitle" # sort key

    attribute {
        name = "UserId"
        type = "S"
    }
    attribute {
        name = "GameTitle"
        type = "S"
    }
}