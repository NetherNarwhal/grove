exports.handler = async (event, context, callback) => {
    const  name = event.name || 'World';
    const response = `Hello, ${name}!`;
    return response;
};