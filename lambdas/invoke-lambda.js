const { LambdaClient, InvokeCommand } = require("@aws-sdk/client-lambda");
const client = new LambdaClient({ region: "us-east-1" });

// This lambda invokes another lambda, as specified in the input event.
exports.handler = async (event, context, callback) => {
    const params = {
        FunctionName: event.name ?? "hello",
        InvocationType: event.type ?? "RequestResponse",
        Payload: JSON.stringify({}),
    };

    try {
        const lambdaResponse = await client.send(new InvokeCommand(params));
        const response = JSON.parse(new TextDecoder().decode(lambdaResponse.Payload));
        return response;
    } catch (error) {
        console.error(error);
        throw error;
    }
};