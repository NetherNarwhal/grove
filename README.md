Grove is a single-file node application, with no dependencies, that emulates AWS services entirely on your computer. This is perfect for folks looking to quickly prototype new ideas or just learn the basics of AWS without needing to get an AWS account, hand over your credit card information, or worry about the complexity of options. The emulated AWS services are able to be accessed and managed via the built-in Grove Console UI or standard AWS APIs, all locally on your computer; no network or internet needed. The latter allows most AWS related tools, such as the AWS CLI, AWS SDK, Infrastructure as Code (IaC) products like OpenTofu, Terraform, etc. to work with Grove.

Grove currently emulates several AWS serverless resources, including:
* Lambda functions (Nodejs and Python)
* API Gateway Rest APIs
* SQS queues
* DynamoDB tables

Support is currently very basic, but more is being added all the time.

Caveats:
* Grove is meant for rapid prototyping and should not be used to run a production system or untrusted code.
* While Grove itself doesn't require any dependencies, any modules used by your lambdas, such as the AWS SDK, need to be installed.
* Grove does not persist any state, even DynamoDB data, so when the process is stopped all state is lost. This may be configurable in the future.

If you need a more full featured AWS emulator I recommend using a tool like [LocalStack](https://github.com/localstack/localstack) or the free tier of AWS itself.

## Getting Started
* Ensure you have [Nodejs](https://nodejs.org) installed.
    * If you plan to run lambdas written in Python, you'll also need [Python](https://www.python.org) installed as well.
* Download `grove.js` from this repository to anywhere on you computer.
* From the command line in the same directory you downloaded `grove.js` to start Grove using `node grove.js`.

That's it. Grove is now ready for you to create and run supported AWS resources.

## Using the Grove Console UI
Open the [Grove Console UI](http://console.localhost:3000) in your browser. By default the URL is `http://console.localhost:3000`, however this can be configured. When starting Grove it should output the correct URL.

In the UI, you can see what resources you have currently running and manage them.

## Using the Grove initialization file
If you'd like to configure Grove and/or define resources for it to create each time it starts you can create an initialization file. Simply create a json file in the directory you start Grove within called `grove.json`. Everything in this file is optional.

Example `grove.json`:
```json
{
    "account": "000000000000",
    "region": "us-east-1",
    "lambdas": {
        "hello": { "handler": "hello.handler", "vars": { "NAME": "Larry" }},
    },
    "apis": {
        "example": {
            "dev": {
                "/hello": { "GET": "hello" },
            }
        }
    },
    "tables": {
        "GameScores": {
            "hashKey": "UserId",
            "rangeKey": "GameTitle",
            "attributes": { "UserId": "S", "GameTitle": "S" }
        }
    },
}
```

The format of the resources in this file are intentionally simple to ensure the file does not get too complicated. If you need advanced options, please use the AWS API options below.

### Elements within the file
TODO: Document


### Comments
The file is standard JSON, which does not typically support comments, however Grove is written to ignore any lines in the file that start with `//`. It will not handle comments with non-whitespace before it on the line.

## Using the AWS CLI with Grove
If you have the AWS CLI installed on your computer it can be used to administer Grove as well. By default it'll try to connect to the actual AWS cloud environment rather than Grove. To get it pointed to the right place we need to set a few environment variables. In the command line/terminal, run the following on Linux or Mac:
```bash
export AWS_ENDPOINT_URL=http://localhost:3000
export AWS_ACCESS_KEY_ID=null
export AWS_SECRET_ACCESS_KEY=null
export AWS_DEFAULT_REGION=us-east-1
```
For windows, use `SET` (or `setx` if you want make them permanent) instead of `export`.

* `AWS_ENDPOINT_URL` variable tells the AWS CLI to point to Grove instead of AWS itself. You don't have to set this variable, but the alternative is to include it in every CLI command which is a lot of typing. Note that the port number, `3000` in this case, is configurable and you may have changed it. When Grove starts up it will tell you which port it is currently using.
* `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` are normally used by the CLI to pass your authentication/login information to AWS. Grove doesn't need any authentication/login info, however the CLI still requires these to be set so we are just putting something there to make the CLI happy.
* `AWS_DEFAULT_REGION` (or `AWS_REGION` for most other AWS tools and SDKs) is optional, however if you don't set a default then the CLI will expect you to provide it for each command. Grove doesn't really support different regions or the region concept at all, however it is needed to conform to the AWS interface.

You now should be able to run standard CLI commands, such as `aws s3 ls`. Note that these exports are terminal session specific, so you may need to set them each time you open a new terminal window.

## Using Infrastructure as Code (IaC) tools (OpenTofu, Terraform, etc) with Grove
Unfortunately, you'll need to use a specially configured provider for Grove in order for IaC tools to call Grove instead of trying to go to AWS. Below is an example of that.

```terraform
variable "grove_endpoint" {
    type = string
    default = "http://localhost:3000"
}

provider "aws" {
    region = "us-east-1"
    access_key = "null"
    secret_key = "null"
    s3_use_path_style = true
    skip_credentials_validation = true
    skip_metadata_api_check = true

    endpoints {
        acm             = var.grove_endpoint
        apigateway      = var.grove_endpoint
        cloudwatch      = var.grove_endpoint
        dynamodb        = var.grove_endpoint
        events          = var.grove_endpoint
        iam             = var.grove_endpoint
        kinesis         = var.grove_endpoint
        kms             = var.grove_endpoint
        lambda          = var.grove_endpoint
        logs            = var.grove_endpoint
        s3              = var.grove_endpoint
        secretsmanager  = var.grove_endpoint
        sns             = var.grove_endpoint
        sqs             = var.grove_endpoint
        stepfunctions   = var.grove_endpoint
        sts             = var.grove_endpoint
    }
}
```

Beyond that the content should remain pretty much the same.

## Hot deploying lambdas
Normally, when creating a Lambda using the AWS API (or anything that uses it) you will need to either pass the code, typically in a zip file, or upload it to an S3 bucket then reference that bucket. Since Grove is intended for rapid prototyping that build process can get annoying. With Grove, you just reference the code where it is so nothing is really "deployed". This happens automatically when using the Grove configuration file or Console UI, however when using the CLI, SDK, or IaC tools you will need to hijack some other parameters to get it to work.

For lambdas, specify `hot-reload` for `s3_bucket` and the path to the directory containing your lambda's source code in `s3_key`.

```terraform
locals {
    # Absolute path to the directory with lambda. Can be "${path.cwd}" if same directory.
    lambda_path = "/home/xyz/dev/my-lambda/"
}

resource "aws_lambda_function" "hello" {
    s3_bucket        = "hot-reload"
    s3_key           = local.lambda_path
    source_code_hash = filebase64sha256("${local.lambda_path}/hello.js")
    function_name    = "hello"
    description      = "Takes in a name and sends a response saying hello to it"
    role             = aws_iam_role.<your role resource>.arn # Definition not included in this snippet
    handler          = "hello.handler"
    runtime          = "nodejs20.x"
}
```

## Static API Gateway IDs
Since Grove does not support Route 53 custom domains this can be somewhat alleviated by using a static id for API Gateway Rest Apis. This is automatically done with creating them via the Grove initialization file, however it gets a little tricky when creating an API via the AWs APIs. We do this via a custom tags.

```terraform
resource "aws_api_gateway_rest_api" "example_api" {
    name        = "ExampleAPI"
    description = "API for Example Lambda Function"

    # define your other settings here

    tags = {
        # Special tag for a static API ID. Will host it at: http://example.execute-api.us-east-1.localhost:3000/
        "_custom_id_" = "example"
    }
}
```

## Building Grove
TODO: Document