const http = require('http');
const { URL } = require('node:url');
const querystring = require('querystring');
const crypto = require('node:crypto');
const fs = require('fs');
const fsp = require('fs').promises;
const { spawn, execSync } = require('child_process');
const readline = require('readline');

const global = {
    port: 3000,
    account: "000000000000",
    region: "us-east-1",
    staticApiId: "_custom_id_", // Special tag used to set the Gateway API ID's to a known value.
    lambdaHotReloadBucket: "local",
    launch: {
        "nodejs": "node",
        "python": "python3",
    }
}

// TODO: Convert this into a cloudwatch type service to manage logging and logs.
const log = new class Logger {
    info(message) { console.log(`${new Date().toISOString()}: ${message}`); }
    warn(message) { console.warn(`${new Date().toISOString()}: WARN: ${message}`); }
    error(message) { console.error(`${new Date().toISOString()}: ERROR: ${message}`); }
}

// Overly simple XML serializer. Doesn't handle attributes or circular references.
const XML = new class {
    stringify(obj) {
        let xml = "";
        for (const key in obj) {
            const value = obj[key];
            if (value === undefined || value === null) {
                xml += `<${key}/>`;
            } else if (Array.isArray(value)) {
                xml += `<${key}>`;
                for (const item of value) xml += XML.stringify(item);
                xml += `</${key}>`;
            } else if (typeof value === "object") {
                xml += `<${key}>${this.stringify(value)}</${key}>`;
            } else if (typeof value === "string") {
                // Escape special characters within XML. >'" (or &gt; &apos; &quot;) shouldn't need to be escaped in text.
                const encoded = value.replace(/[<&]/g, function(m) {
                    switch (m) {
                        case '<': return '&lt;';
                        case '&': return '&amp;';
                    }
                });
                xml += `<${key}>${encoded}</${key}>`;
            } else {
                xml += `<${key}>${value}</${key}>`;
            }
        }
        return xml;
    }
}
// console.log(XML.stringify({test: "value", array: [1, 2, 3], arrayobj: [{name:"A"},{name:"B"}], nested: {key: "value"}}));
// console.log(XML.stringify({testJson: JSON.stringify({test: "<value>", array: [1, 2, 3]})}));

const generator = new class Generator {
    static lAlphaNum = "abcdefghijklmnopqrstuvwxyz0123456789";
    static uAlphaNum = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    // Typically used to generate things like request ids. Format: df320854-5db9-4582-b3c0-a5b209562728
    uuid() { return crypto.randomUUID(); }

    // Lowercase alphanumeric ids, like those used by the api gateway. ex: "1kqoyoqwwz"
    lowerAlphaNumeric(length=10) {
        let result = "";
        for (let i = 0; i < length; i++)
            result += Generator.lAlphaNum.charAt(Math.floor(Math.random() * Generator.lAlphaNum.length));
        return result;
    }

    // IAM ID's are 21 characters long including the 4 character prefix: AIDA=user, AROA=role, ANPA=policy, etc.
    // See https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_identifiers.html#identifiers-unique-ids
    iam(prefix, length=21) {
        let result = prefix;
        length -= prefix.length;
        for (let i = 0; i < length; i++)
            result += Generator.uAlphaNum.charAt(Math.floor(Math.random() * Generator.uAlphaNum.length));
        return result;
    }

    // Timestamps. Of course, AWS uses a variety of different formats because why not:
    //   IAM:     2024-05-14T22:59:23.990000Z
    //   Lambda:  2024-05-14T22:59:23.990999+0000 (LastModified)
    //   Gateway: 1715727569.0 (created date)

    // Format: 2024-05-14T22:59:23.123Z
    timestamp() { return (new Date()).toISOString(); }
    // Format: 1715727569.0
    epoch() { return Date.now() / 1000; }
}

class Router {
    constructor(routes) {
        this.routes = {};
        if (routes) for (const route of routes) this.add(route.url, route.method, route.action);
    }

    static Response = class Response {
        constructor(req, res) {
            this.req = req;
            this.res = res;
            this.requestId = req.headers["x-amzn-requestid"] ?? req.headers["x-amz-requestid"] ;
            if (this.requestId) {
                res.setHeader("x-amzn-request-id", this.requestId);
                res.setHeader("x-amz-request-id", this.requestId); // Old one used by S3
            }
        }
        set(key, value) { this.res.setHeader(key, value); return this; }
        type(type) { this.res.setHeader("Content-Type", type); return this; }
        send(code, data, operation) {
            this.res.statusCode = code;
            if (typeof data === "string") {
                this.res.end(data);
            } else {
                this.type("application/json");
                this.res.end(JSON.stringify(data));
            }
            if (operation) log.info(`${operation} => ${code}`);
        }
        sendXML(code, wrapper, xmlns, data, operation) {
            this.res.statusCode = code;
            this.type("text/xml");
            this.res.end(`<?xml version='1.0' encoding='utf-8'?>\n<${wrapper} xmlns="${xmlns}">${XML.stringify(data)}<ResponseMetadata><RequestId>${this.requestId}</RequestId></ResponseMetadata></${wrapper}>`);
            if (operation) log.info(`${operation} => ${code}`);
        }
        sendError(code, message, operation) {
            this.send(code, { error: message });
            if (typeof message !== "string") message = JSON.stringify(message);
            if (operation) log.error(`${operation} => ${code}\n  ${message}`);
        }
    }

    any(url, action) { this.add(url, "ANY", action); return this; } // Matches any method.
    get(url, action) { this.add(url, "GET", action); return this; }
    post(url, action) { this.add(url, "POST", action); return this; }
    put(url, action) { this.add(url, "PUT", action); return this; }
    head(url, action) { this.add(url, "HEAD", action); return this; }
    patch(url, action) { this.add(url, "PATCH", action); return this; }
    delete(url, action) { this.add(url, "DELETE", action); return this; }
    // Excluding options, and trace as they are rarely used in APIs.

    // IDEA: For performance, may want to partition routes based on first part of path (ex. "/restapis").
    // TODO: Handle the special {proxy+} (greedy path variable) use case?
    add(url, method, action) {
        const params = [];
        const pattern = url.replace(/{([^}]+)}/g, (match, paramName) => {
            params.push(paramName);
            return "([^\/]+)";
        });
        // See if this pattern already exists and we just need to add the method/action to it.
        const route = this.routes[pattern];
        if (route) { route[method] = action; return; }
        this.routes[pattern] = { "pattern": new RegExp("^" + pattern + "$"), params, [method]: action };
    }

    replaceAll(routes) {
        this.routes = {};
        if (routes) for (const route of routes) this.add(route.url, route.method, route.action);
    }

    #getBody(req) {
        return new Promise((resolve, reject) => {
            let data = "";
            req.on("data", (chunk) => { data += chunk.toString(); });
            req.on("end", () => { resolve(data); });
            req.on("error", (err) => { reject(err); });
        });
    }

    async route(req, res) {
        try {
            const url = new URL(req.url, "http://" + req.headers.host); // Can't use req.url as it has querystring.
            let action;
            const params = {};
            // IDEA: Turn the list of routes into an array and sort them so the most specific routes are first?
            for (const key in this.routes) {
                const route = this.routes[key];
                const match = url.pathname.match(route.pattern);
                if (match) {
                    action = route[req.method]; // Action is associated to the method.
                    if (!action) action = route["ANY"]; // This special method will match any method.
                    if (!action) { // Found route, but no matching method.
                        const message = `No method for [${req.method}] ${req.url}`;
                        log.warn(message);
                        res.statusCode = 500;
                        res.end(message);
                        return;
                    }
                    route.params.forEach((param, index) => { params[param] = match[index + 1]; });
                    break;
                }
            }
            if (!action) { // Didn't find route.
                const message = `No route for [${req.method}] ${req.url}`;
                log.warn(message);
                res.statusCode = 500;
                res.end(message);
                return;
            }

            // IDEA: Instead of actually doing the routing, we may just want to pass back the context.
            // This would allow API Gateway to do the routing.
            const hasBody = !(req.method === "GET" || req.method === "HEAD");
            const request = {
                host: url.host, // Host name and port, if supplied.
                method: req.method, // Http method (GET, POST, etc).
                path: url.pathname, // URL without the host or querystring.
                headers: req.headers, // Http headers.
                query: Object.fromEntries(url.searchParams.entries()), // Query strings.
                params, // Variables from the path.
                body: (hasBody ? (await this.#getBody(req)) : undefined), // TODO: May be an issue for streaming?
            };
            const response = new Router.Response(req, res)
            await action(request, response);
            if (!res.writableEnded) res.end();

        } catch (e) {
            console.error(e);
            log.error(`Error in route for [${req.method}] ${req.url}: ${JSON.stringify(e)}`); // FIXME: Why does error info not log?
            res.statusCode = 500;
            res.end("Internal Server Error");
        }
    }
}

const routers = { }


// **************** AWS RESOURCES ****************

class Role {
    constructor(name, options) {
        this.name = name;
        this.assumeRolePolicyDocument = {}; // TODO: Is a role valid without one?
        this.arn = `arn:aws:iam::${global.account}:role/${name}`;
        this.id = generator.iam("AROA");
        this.createDate = generator.timestamp();
        // Optional: (most have no effect and are just there to make terraform/tofu happy)
        this.maxSessionDuration = 3600;
        // this.path = "/";
        // this.version = "2010-05-08"
        this.setOptions(options);
    }

    setOptions(options) { // Throw an exception if there are problems.
        if (options.AssumeRolePolicyDocument)
            this.assumeRolePolicyDocument = JSON.parse(options.AssumeRolePolicyDocument);
        if (options.MaxSessionDuration) this.maxSessionDuration = options.MaxSessionDuration;
    }

    getInfo() {
        return { Role: { Path: "/", RoleName: this.name, RoleId: this.id, Arn: this.arn, CreateDate: this.createDate, AssumeRolePolicyDocument: JSON.stringify(this.assumeRolePolicyDocument), MaxSessionDuration: this.maxSessionDuration } };
    }
}

class Lambda {
    static stateReason = {
        Idle: "The function is active and is ready to process events.",
        Creating: "The function is being created.",
        Restoring: "The function is being restored from a snapshot or previous version.",
        EniLimitExceeded: "The function couldn't be created because the AWS account reached the limit for elastic network interfaces.",
        InsufficientRolePermissions: "The execution role that's associated with the function doesn't have the necessary permissions.",
        InvalidConfiguration: "The function's configuration is invalid.",
        InternalError: "An error occurred.",
    }

    constructor(name, handler, runtime) {
        // https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/client/lambda/command/GetFunctionCommand/
        // Required:
        this.name = name || generator.lowerAlphaNumeric(10); // FunctionName
        this.handler = handler ?? this.name.toLowerCase() + ".handler"; // The file and method name to invoke. ex. hello.handler
        this.runtime = runtime ?? "nodejs"; // ex. nodejs20.x
        // Generated:
        this.arn = `arn:aws:lambda:${global.region}:${global.account}:function:${name}`; // FunctionArn
        this.lastModified = generator.timestamp();
        // Optional: (most have no effect and are just there to make terraform/tofu happy)
        this.description = "";
        this.role = undefined; // The role's arn. Create one if not specified? ex. arn:aws:iam::000000000000:role/lambda_role
        this.memorySize = 128;
        this.timeout = 3; // 3 seconds.
        this.s3Bucket = global.lambdaHotReloadBucket; // Assume they are using hot reload.
        this.s3Key = process.cwd(); // Assume they are using hot reload. Full file system path. ex. "/home/user/dev"
        this.logGroup = `/aws/lambda/${name}`;
        this.logStream = this.getLogStreamName();
        this.state = "Active"; // Pending, Active, Inactive, Failed
        this.stateReasonCode = "Idle"; // See Lambda.stateReason for options.
        this.tags = {};
        this.environment = { variables: {}, }; // key:value pairs. Official also has a error property?
        this.version = "$LATEST"; // The version of the function. ex. "$LATEST"
    }

    // TODO: Should have a logger service (a.k.a. CloudWatch) generate this instead?
    getLogStreamName() {
        const date = new Date();
        const year = date.getFullYear();
        const month = String(date.getMonth() + 1).padStart(2, '0'); // Months are 0-based in JavaScript
        const day = String(date.getDate()).padStart(2, '0');
        // format: YYYY/MM/DD/[$LATEST]1987583d27255c2f2654bedc38db7513 < last part is Execution environment GUID, which we don't use
        return `${year}/${month}/${day}/[${this.version}]${generator.lowerAlphaNumeric(32)}`;
    }

    // TODO: Log
    //  START RequestId: $ Version: $
    //  END RequestId: $
    //  REPORT RequestId: $ Duration: # ms Billed Duration: # ms Memory Size: # MB Max Memory Used: # MB Init Duration: # ms
    // note: duration excludes init (which not sure I can do below), billing is round up to nearest 1ms and includes init.
    invoke(event, requestId=generator.uuid()) {
        // Found a site that says you can use this to cap memory, but I can't find in the official docs.
        // https://colinchjs.github.io/2023-10-10/08-51-46-222704-limiting-the-resources-cpumemory-of-a-child-process-in-nodejs/
        const args = {
            cwd: this.s3Key,
            env: {
                // TODO: This works to fake out CLI, will it work for the lambdas?
                AWS_ENDPOINT_URL: "http://localhost:" + global.port,
                AWS_ACCESS_KEY_ID: "na",
                AWS_SECRET_ACCESS_KEY: "na",
                AWS_DEFAULT_REGION: global.region, // For Boto. TODO: Do we want to fake out region, probably not?
                AWS_REGION: global.region, // Preferred. For the other SDKs.
                ...this.environment.variables
            },
            stdio: ["pipe", "pipe", "pipe", "ipc"],
//            timeout: this.timeout * 1000,
        }
        const inputData = JSON.stringify({
            event,
            context: {
                callbackWaitsForEmptyEventLoop: true,
                functionVersion: this.version,
                functionName: this.name,
                memoryLimitInMB: this.memorySize,
                logGroupName: this.logGroup, // "/aws/lambda/hello"
                logStreamName: this.logStream,
                invokedFunctionArn: this.arn,
                awsRequestId: requestId, // "dfeec625-0e16-4092-ad04-07c0b0419473"
            }
        });

        let launchCmd = global.launch[this.runtime];
        if (!launchCmd) {
            if (this.runtime.startsWith("nodejs")) launchCmd = global.launch.nodejs;
            else if (this.runtime.startsWith("python")) launchCmd = global.launch.python;
            else {
                return {
                    StatusCode: 400,
                    FunctionError: 'InvalidRuntimeException',
                    LogResult: 'Invalid runtime provided',
                    Payload: JSON.stringify({ errorMessage: 'Invalid runtime provided' }),
                    ExecutedVersion: this.version,
                }
            }
        }

        // Create a child process to invoke the lambda.
        // IDEA: Is it possible to cache the child process like AWS does with lambdas? Can't because input data is hardcoded?
        // IDEA: Should we use a worker instead?
        // https://nodejs.org/api/child_process.html#child_processspawncommand-args-options
        let child = spawn(launchCmd, ["-"], args);
        const handlerSplit = this.handler.split(".");
        const lambdaFile = handlerSplit[0], lambdaMethod = handlerSplit[1];

        // Waiting for lambda output is async, so pass back a promise for the caller to wait on.
        const self = this; // `this` won't exist inside the promise.
        return new Promise((resolve, reject) => {
            let done = false;

            function getErrorMsg(code, error) {
                const response = { StatusCode: code, FunctionError: error.name ?? "Unhandled", ExecutedVersion: self.version };
                if (error.isError) {
                    response.LogResult = `${error.name}: ${error.message}\n${error.stack}`;
                    response.Payload = JSON.stringify({ errorMessage: error.message });
                } else {
                    response.LogResult = response.Payload = error;
                }
                return response;
            }

            // Technically, response can be a string but API gateway expects the following structure (! required):
            //  { !statusCode:200, !body: "stringified json", headers: {}, isBase64Encoded:false }
            // TODO: Should this all be StatusCode (per invoke API) or statusCode (what you'd write in the lambda)?
            function processResponseMessage(message) {
                if (message.isError) {
                    // log.error(`Lambda error: ${message.name}: ${message.message}\n${message.stack}`);
                    done = true;
                    reject(getErrorMsg(500, message));
                } else {
                    log.info(`Lambda response: ${JSON.stringify(message)}`);
                    done = true;
                    if (message.statusCode && message.body) {
                        resolve({ // TODO: message.headers?
                            StatusCode: message.statusCode,
                            Payload: message.body,
                            ExecutedVersion: self.version,
                            // TODO: Is this complete?
                        });
                    } else {
                        resolve({
                            StatusCode: 200,
                            Payload: JSON.stringify(message),
                            ExecutedVersion: self.version,
                            // TODO: Is this complete?
                        });
                    }
                }
            }

            if (this.runtime.startsWith("nodejs")) {
                // There are several ways a nodejs lambda can respond:
                // 1. Use context.succeed(string) or context.done(error, result). Both deprecated, won't support.
                // 2. Using callback (3rd arg), which takes 2 arguments: error (error obj or string), result (any type).
                // 3. Just doing a return. This is the most modern and preferred way.
                // The callback takes precedence over the return.
                child.stdin.write( // WARNING: Using Stdin is limited to about 2MB. May need to use another approach for inputData.
                    // TODO: Switch to import if modules is set in package.json or handler file extension is mjs?
                    // IDEA: Support `awslambda.streamifyResponse(` wrapping the handler? Or lambda extensions?
                    `const handler = require("./${lambdaFile}.js");
                    async function launch(process, data) {
                        let response;
                        const callback = (e, result) => {
                            if (e) {
                                if (e.name) response = { isError:true, name:e.name, message:e.message, stack:e.stack };
                                else response = { isError:true, name:"Error", message:e };
                                console.error(e); // Use stderr so it gets added to lambda's logs.
                            } else response = result;
                        };
                        try {
                            const callResponse = await handler.${lambdaMethod}(data.event, data.context, callback);
                            if (!response) response = callResponse;
                        } catch (e) {
                            response = { isError:true, name:e.name, message:e.message, stack:e.stack };
                            console.error(e); // Use stderr so it gets added to lambda's logs.
                        } finally {
                            process.send(response);
                        }
                    }
                    launch(process, ${inputData});`
                );

                child.stdin.end();
                // Logs from console.log for both lambda and launcher (avoid in launcher, use send instead).
                child.stdout.on("data", (data) => { log.info(`stdout: ${data.toString().trimEnd()}`) });
                // Errors from console.error for both lambda and launcher (avoid in launcher, use send instead).
                child.stderr.on("data", (data) => { log.error(`stderr: ${data.toString().trimEnd()}`) });
                // Messages from launcher using IPC's process.send.
                child.on("message", (message) => { processResponseMessage(message); });
                // This as a failsafe in case something goes wrong with launcher and it never responds.
                child.on("close", (code) => {
                    if (done) return;
                    const message = `Lambda exited early with code ${code}`;
                    // log.error(message);
                    reject(getErrorMsg(500, message));
                });

            } else if (this.runtime.startsWith("python")) {
                // Currently, communicating with stdout. Since it shared for output from lambda and the return it can be difficult.
                // Another option may be named pipes:
                // https://levelup.gitconnected.com/inter-process-communication-between-node-js-and-python-2e9c4fda928d
                // But implementation is different between OSs (not all differences are shown? this was from AI).
                // Linux:
                //   const fs = require('fs'); const pipePath = '/path/to/named/pipe';
                //   fs.mkfifoSync(pipePath);
                //   const readStream = fs.createReadStream(pipePath);
                //   const writeStream = fs.createWriteStream(pipePath);
                // Windows:
                //   const fs = require('fs'); const path = require('path');
                //   const pipePath = path.join('\\\\.\\pipe', 'myNamedPipe');
                //   const readStream = fs.createReadStream(pipePath);
                //   const writeStream = fs.createWriteStream(pipePath);

                // A python lambda can only response by returning a value from the handler or raining an exception.
                // The response does not need to be in json, however that format is required when responding to an API Gateway.
                // TODO: Handle error and different responses (error or success, see below).
                child.stdin.write(
                    `import json\n` +
                    `import traceback\n` +
                    `try:\n` +
                    `  from ${lambdaFile} import ${lambdaMethod}\n` +
                    `  inputData = json.loads('${inputData}')\n` +
                    // Response can be json (regular or response format: statusCode,headers,body,isBase64Encoded), an exception (handled by catch), or primitive (ex. string).
                    `  response = ${lambdaMethod}(inputData.get("event"), inputData.get("context"))\n` +
                    `  if not isinstance(response, (int, float, str, bool, type(None))): response = json.dumps(response)\n` +
                    `  print(f"$!{response}")\n` +
                    `except Exception as e:\n` +
                    `  trace = traceback.format_exc()\n` +
                    `  print("$!" + json.dumps({"isError":True, "name":e.__class__.__name__, "message":str(e), "trace":trace}))`
                );
                child.stdin.end();
                // Errors from console.error for both lambda and launcher (avoid in launcher, use send instead).
                child.stderr.on("data", (data) => { log.error(`stderr: ${data.toString().trimEnd()}`) });

                // Using this for logs from print() for both lambda and launcher. Launcher output prefixed with `$!`.
//                 child.stdout.on("data", (data) => {
// console.log("**stdout:", data.toString());
//                     data = data.toString().trimEnd();
//                     if (data.startsWith("$!")) {
//                         processResponseMessage(data.substring(2));
//                     } else {
//                         log.info(`stdout: ${data}`)
//                     }
//                 });

                const stdout = readline.createInterface({
                    input: child.stdout,
                    output: process.stdout,
                    terminal: false
                });
                stdout.on('line', (line) => {
                    console.log("**stdout:", line);
                    if (line.startsWith("$!")) { // FIXME: Cache it to make sure we get it all, if not a single line json?
                        processResponseMessage(JSON.parse(line.substring(2)));
                    } else {
                        log.info(`stdout: ${line}`)
                    }
                });

                // This as a failsafe in case something goes wrong with launcher and it never responds.
                child.on("close", (code) => {
                    if (done) return;
                    const message = `Lambda exited early with code ${code}`;
                    // log.error(message);
                    reject(getErrorMsg(500, message));
                });

            } else {
                reject({
                    StatusCode: 400,
                    FunctionError: 'InvalidRuntimeException',
                    LogResult: 'Invalid runtime provided',
                    Payload: JSON.stringify({ errorMessage: 'Invalid runtime provided' }),
                    ExecutedVersion: this.version,
                });
            }
        });
    }
}

// https://github.com/aws/aws-sdk-js/tree/master/apis
// https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/client/api-gateway/
// Advertises endpoints on http://<api-id>.execute-api.<region>.localhost:<port>/<stage>/<path>
//   Edge type urls don't contain region, but regional and private ones do.
// RestAPI > Stage > Deployment > Resource > Method > Integration (can call Lambda)
// API Gateway is weird in that you add resources, but they don't take effect until you create a deployment
// (which takes a snapshot of what is currently created) and then attach that deployment to a stage.
// Put another way, resources are added to a WIP deployment.
// TODO: Add support for deployments.
class APIGateway {
    static supportedMethods = ["GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS", "ANY"]; // ANY is a non-standard catch-all.

    constructor(name, tags={}) {
        this.name = name;
        this.id = tags[global.staticApiId] ?? generator.lowerAlphaNumeric(10);
        this.createDate = generator.epoch();
        this.stages = {
            // "dev": { // Default stage?
            //     "createdDate": generator.timestamp(),
            //     "lastUpdatedDate": generator.timestamp(),
            //     "stageName": "dev",
            //     "deploymentId": generator.lowerAlphaNumeric(10),
            //     "cacheClusterEnabled": false,
            //     "cacheClusterSize": "0.5",
            //     "cacheClusterStatus": "NOT_AVAILABLE",
            //     "methodSettings": {},
            //     "variables": {},
            // }
        };
        this.deployments = {};

        // This resource stuff isn't really needed for my simplistic regex implementation, but
        // AWS exposed its implementation details so they are part of the interface.
        this.root = generator.lowerAlphaNumeric(10); // Every api has a default path resource. Path is always "/".
        this.resources = { [this.root.id]: this.root };

        // Optional: most have no effect and are just there to conform to the AWS API
        this.description = ""
        this.tags = tags;
        // Ignored:
        // "apiKeySource": "HEADER",
        // "endpointConfiguration": {"types": ["EDGE"]}, // Can be Edge, Regional, or Private
        // "disableExecuteApiEndpoint": false,
        // "rootResourceId": "1kqoyoqwwz"

        // Internal implementation reuses the Router.
        this.router = new Router();
        this.domain = `${this.id}.execute-api.${global.region}.localhost`;
        routers[this.domain] = this.router;
    }

    createDeployment() {

    }

    createStage(stageName, deploymentId) {

    }

    // Called via AWS APIs.
    createResource(path, parentId) {
        // {"id": "21vhnwztri", "parentId": "1kqoyoqwwz", "pathPart": "hello", "path": "/hello", "resourceMethods": {"ANY": {"httpMethod": "ANY", "authorizationType": "NONE", "apiKeyRequired": false, "methodResponses": {}, "methodIntegration": {"type": "AWS_PROXY", "httpMethod": "POST", "uri": "arn:aws:apigateway:us-east-1:lambda:path/2015-03-31/functions/arn:aws:lambda:us-east-1:000000000000:function:hello/invocations", "passthroughBehavior": "WHEN_NO_MATCH", "timeoutInMillis": 29000, "cacheNamespace": "21vhnwztri", "cacheKeyParameters": []}}}}]
        return {
            id: generator.lowerAlphaNumeric(10),
            path,
            parentId,
            // TODO: Implement the rest?
        };

        // TODO: Update the router?
    }

    // Not AWS standard. Called internally, typically from config file init.
    addRoute(stageName, path, method, target) {
        let stage = this.stages[stageName];
        if (!stage) { this.stages[stageName] = stage = { stageName, paths:{}, variables:{}, }; } // Add stage, if needed.
        let pathRoute = stage.paths[path];
        if (!pathRoute) { stage.paths[path] = pathRoute = {}; }  // Add path, if needed.
        pathRoute[method] = target;
        this.router.add(`/${stageName}${path}`, method, this.onCall.bind(this));
        log.info(`API Gateway added route: /${stageName}${path} ${method} => ${target}`);
    }

    async onCall(req, res) {
        // Get stage from path.
        const pos = req.path.indexOf("/", 1);
        const stageName = req.path.substring(1, pos);
        const stage = this.stages[stageName];
        if (!stage) {
            const message = `No stage for ${stageName}.`;
            res.send(404, message, "apiGateway.onCall"); log.error(message);
            return;
        }
        const path = req.path.substring(pos);
        const pathLookup = stage.paths[path];
        if (!pathLookup) {
            const message = `No path for ${pathLookup}.`;
            res.send(404, message, "apiGateway.onCall"); log.error(message);
            return;
        }
        let target = pathLookup[req.method];
        if (!target) target = pathLookup["ANY"];
        if (!target) {
            const message = `No target for ${req.method} ${req.path}.`;
            res.send(404, message, "apiGateway.onCall"); log.error(message);
            return;
        }
        const lambda = account.getLambda(target); // FIXME: Which account? Need to store that with gateway?
        if (!lambda) {
            const message = `No lambda for ${target}.`;
            res.send(404, message, "apiGateway.onCall"); log.error(message);
            return;
        }

        const event = {
            resource: "/{proxy+}", // TODO: ???
            path: path,
            httpMethod: req.method,
            requestContext: {
                "accountId": global.account,
                "apiId": this.id,
                "resourcePath": path,
                "domainPrefix": this.id,
                "domainName": this.domain,
                "resourceId": "na", // TODO: ???
                "requestId": res.requestId,
                "identity": { "accountId":global.account, "sourceIp":"172.19.0.1" }, // TODO: ???
                "httpMethod": req.method,
                "protocol": "HTTP/1.1", // TODO: ???
                "requestTime": "16/May/2024:00:26:31 +0000", // TODO: ???
                "requestTimeEpoch": 1715819191977,
                "authorizer": {},
                "path": path,
                "stage": stageName,
            },
            headers: req.headers,
            multiValueHeaders: {}, // Not supported.
            queryStringParameters: req.query, // { "postcode": 12345 }
            multiValueQueryStringParameters: {}, // TODO: Always set this to {} or null, or have it match querystring?
            pathParameters: req.params, // {"proxy":"hello"}
            body: req.body,
            stageVariables: {}, // TODO: Get these from stage?
            isBase64Encoded: false,
        };
        let response;
        try {
            response = await lambda.invoke(event, res.requestId);
        } catch (e) {
            response = { StatusCode: 500, Payload: "Internal Server Error" };
        }
console.log("invoke Response:", response);

        // Copy over headers from response into res.
        if (!response) {
            response = { StatusCode: 500, Payload: "Internal Server Error" };
        } else if (response.Headers && response.Headers.length > 0)
            for (const key in response.Headers) res.setHeader(key, response.Headers[key]);

console.log("ResponseCode:", response.StatusCode);
        res.send(response.StatusCode, response.Payload);
    }
}

class Table {
    constructor(name) {
        this.name = name;
        this.arn = `arn:aws:dynamodb:${global.region}:${global.account}:table:${name}`;
        this.id = generator.uuid;
        this.createDate = generator.epoch();
        this.hashKey = undefined; // a.k.a. partition key. Must be unique within a table. Required.
        this.hashKeyType = "S"; // S | N | B
        this.rangeKey = undefined; // a.k.a. sort key. Must be unique within a hashKey, but not table. Optional.
        this.rangeKeyType = "S"; // S | N | B
        this.attributes = {}; // { "name": "S" }
        this.localIndexes = {}; // LocalSecondaryIndex. Up to 5.
        // {
        //     "name": { // Must be unique in table. HashKey is same as table.
        //         rangeKey = undefined; // a.k.a. sort key. Must be unique within a hashKey(?), but not index. Optional.
        //         rangeKeyType = "S"; // S | N | B
        //         projectionType: "ALL", // "ALL" || "KEYS_ONLY" || "INCLUDE"
        //         attributes: [], // Can't exceed 100 in total for all secondary indexes. Duplicates are considered distinct.
        //     }
        // }
        this.globalIndexes = {}; // GlobalSecondaryIndexes. Up to 20.
        // {
        //     "name": { // Must be unique in table. Has its own hashKey.
        //         hashKey = undefined; // a.k.a. partition key. Must be unique within an index. Required.
        //         hashKeyType = "S"; // S | N | B
        //         rangeKey = undefined; // a.k.a. sort key. Must be unique within a hashKey(?), but not index. Optional.
        //         rangeKeyType = "S"; // S | N | B
        //         projectionType: "ALL", // "ALL" || "KEYS_ONLY" || "INCLUDE"
        //         attributes: [], // Can't exceed 100 in total for all secondary indexes. Duplicates are considered distinct.
        //     }
        // }
        // TODO: TTL
        // TODO: Streams
        this.itemCount = 0;
        this.items = {}; // Essentially the rows, with the hash key as the key.
        this.tags = {};
    }

    getItem(keys, attributes, projection, expressionMap) {
        if (!keys
            || Object.keys(keys).length !== (this.rangeKey ? 2 : 1)
            || !(this.hashKey in keys)
            || !(this.hashKeyType in keys[this.hashKey])
            || (this.rangeKey && ( // If a range key is defined for table, then they must provide one.
                !(this.rangeKey in keys)
                || !(this.rangeKeyType in keys[this.rangeKey])
            ))) return {
                __type: 'ValidationException',
                message: 'The number of conditions on the keys is invalid'
            };

        let item = this.items[keys[this.hashKey]];
        if (this.rangeKey) item = item[keys[this.rangeKey]];

        // TODO: Support for filtering down by attributes (if supplied, otherwise return all). It is a legacy parameter.
        // TODO: Support for projection (and expressionMap), if supplied.

        return { Item: item };
    }

    putItem(item, Expected, ConditionalOperator, ConditionExpression, ExpressionAttributeNames, ExpressionAttributeValues, ReturnValues, ReturnValuesOnConditionCheckFailure) {
        // ConditionExpression - what must be true for update to happen: 
        //   Functions: attribute_exists | attribute_not_exists | attribute_type | contains | begins_with | size
        //   These function names are case-sensitive.
        //   Comparison operators: = | | | | = | = | BETWEEN | IN
        //   Logical operators: AND | OR | NOT
        // ExpressionAttributeNames - ex. {"#P":"Percentile"}
        // ExpressionAttributeValues - ex. { ":avail":{"S":"Available"}, ":back":{"S":"Backordered"}, ":disc":{"S":"Discontinued"} }
        // ConditionalOperator - "AND" || "OR". This is a legacy parameter. Use ConditionExpression.
        // Expected - {<key>: {Value:"", Exists:true, ComparisonOperator:"EQ", AttributeValueList:[]}} This is a legacy parameter. Use ConditionExpression.
        // ReturnValues - NONE | ALL_OLD (other operations support others, but put item is only these 2)
        // ReturnValuesOnConditionCheckFailure - what name says. "ALL_OLD" || "NONE"

        // Validate item has the right primary keys.
        if (!item
            // || Object.keys(item).length !== (this.rangeKey ? 2 : 1)
            || !(this.hashKey in item)
            || !(this.hashKeyType in item[this.hashKey])
            || (this.rangeKey && ( // If a range key is defined for table, then they must provide one.
                !(this.rangeKey in item)
                || !(this.rangeKeyType in item[this.rangeKey])
            ))) return {
                __type: 'ValidationException',
                message: 'The number of conditions on the keys is invalid'
            };

        // Upsert the item.
        const hashKeyVal = item[this.hashKey][this.hashKeyType];
        if (this.rangeKey) {
            const rangeKeyVal = item[this.rangeKey][this.rangeKeyType];
            let foundItem = this.items[hashKeyVal];
            if (!foundItem) {
                foundItem = this.items[hashKeyVal] = {};
                this.itemCount++;
            } else {
                if (!foundItem[rangeKeyVal]) this.itemCount++;
            }
            foundItem[rangeKeyVal] = item;
        } else {
            if (!this.items[hashKeyVal]) this.itemCount++;
            this.items[hashKeyVal] = item;
        }
    }

    deleteItem(item) {
        // TODO: Implement
    }

    // Does not support consistent reads, segments for parallel scans,
    // limit (which is # to evaluate prior to filter not necessarily return), or any pagination (startKey, ex. 1MB limit).
    scan(indexName, attributesToGet, select, limit, filter, expressionAttributeNames, expressionAttributeValues, projection, scanFilter) {
        if (indexName) return { __type: 'ValidationException', message: 'Index not yet supported' };
        // attributesToGet - array of attribute names to return. If not supplied, all are returned.
        // select - "ALL_ATTRIBUTES"(default for table)) | "ALL_PROJECTED_ATTRIBUTES" (default for index) | "SPECIFIC_ATTRIBUTES" | "COUNT". Can't be used with projection unless ALL_PROJECTED_ATTRIBUTES used.
        // limit - max number of items to evaluate (pre-filter). If not supplied, all are returned.
        // filter - applied after scan, before data is returned. ex. "Title = :avail AND Price > :back AND InStock = :disc"
        // expressionAttributeNames - ex. {"#P":"Percentile"}
        // expressionAttributeValues - ex. { ":avail":{"S":"Available"}, ":back":{"S":"Backordered"}, ":disc":{"S":"Discontinued"} }
        // projection - ex. {"ProjectionExpression":"#P, Title", "ExpressionAttributeNames":{"#P":"Percentile"}}
        // conditionalOperator - a legacy parameter. Use FilterExpression.
        // scanFilter - a legacy parameter. Use FilterExpression.

        // Flatten out the items in the table.
        const items = [];
        if (this.rangeKey) {
            for (const hashKey in this.items) {
                for (const rangeKey in this.items[hashKey]) {
                    const row = {
                        [this.hashKey]: { [this.hashKeyType]: hashKey },
                        [this.rangeKey]: { [this.rangeKeyType]: rangeKey }
                    };
                    // TODO: Return other requested attributes.
                    items.push(row);
                }
            }
        } else {
            for (const hashKey in this.items) {
                const row = {
                    [this.hashKey]: { [this.hashKeyType]: hashKey },
                };
                // TODO: Return other requested attributes.
                items.push(row);
            }
        }
        return { Items: items, Count: items.length, ScannedCount: items.length }
    }

    // Allows you to retrieve all items (different range keys) for a given hash key.
    query() {
        // TODO: Implement
    }

    // IDEA: Does this belong here or in the AWS router section?
    getDescription() {
        const keySchema = [{"AttributeName": this.hashKey, "KeyType": "HASH"}];
        if (this.rangeKey) keySchema.push({"AttributeName": this.rangeKey, "KeyType": "RANGE"});
        const attributes = Object.entries(this.attributes).map(([key, value]) => {
            return {"AttributeName": key, "AttributeType": value};
        });

        return {"AttributeDefinitions": attributes, "TableName": this.name, "KeySchema": keySchema, "TableStatus": "ACTIVE", "CreationDateTime": this.createDate, "ProvisionedThroughput": {"LastIncreaseDateTime": 0.0, "LastDecreaseDateTime": 0.0, "NumberOfDecreasesToday": 0, "ReadCapacityUnits": 20, "WriteCapacityUnits": 20}, "TableSizeBytes": 0, "ItemCount": this.itemCount, "TableArn": this.arn, "DeletionProtectionEnabled": false, "Replicas": [], "TableId": this.id}
    }
}

class Queue {
    constructor(name, options) {
        this.name = name;
        this.url = `http://localhost:3000/queues/${name}`;
        this.arn = `arn:aws:sqs:${global.region}:${global.account}:${name}`;
        this.visibilityTimeout = options.VisibilityTimeout ?? 30; //
        this.maximumMessageSize = 262144;
        this.messageRetentionPeriod = 345600;
        this.delaySeconds = 0;
        this.receiveMessageWaitTimeSeconds = 0;
        // RedrivePolicy: "{\"deadLetterTargetArn\":\"arn:aws:sqs:${global.region}:${global.account}:my-dead-letter-queue\",\"maxReceiveCount\":10}",
        this.fifoQueue = false;
        this.fifoThroughputLimit = "perQueue";
        this.contentBasedDeduplication = false;
        this.deduplicationScope = "messageGroup";
        this.kmsMasterKeyId = "alias/aws/sqs";
        this.kmsDataKeyReusePeriodSeconds = 300;
        this.sqsManagedSseEnabled = true;
        this.created = generator.epoch();
        this.lastModified = generator.epoch();
        this.messages = [];
        this.numberOfMessagesNotVisible = 0;
        this.numberOfMessagesDelayed = 0;
        this.tags = {};
    }

    sendMessage(message, options, res) {
        // TODO: Implement

    }

    receiveMessages() {
        // Retrieves up to 10 messages from the queue.
        // TODO: Implement

    }

    deleteMessage(receiptHandle, res) { // Doesn't take messageId.
        // TODO: Implement

    }

    // TODO: Implement batch versions of send and delete.
}

class EventSourceMapping {
    static getMappingsForEventSource(sourceArn) {
        const mappings = [];
        for (const mapping of Object.values(EventSourceMapping.eventSourceMappings)) {
            if (mapping.eventSourceArn === sourceArn) mappings.push(mapping);
        }
        return mappings;
    }

    // https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-lambda-eventsourcemapping.html
    constructor(options) {
        this.id = generator.uuid();
         // FIXME: Which account? Need to store that with gateway?
         // TODO: What happens if not found (assumes they checked this in caller)? How do we set/change this latter?
        this.function = account.getLambda(options.FunctionName);
        this.functionArn = this.function.arn; // Arn of the lambda is officially what we store.
        this.eventSourceArn = options.EventSourceArn; // Arn of the source (ex. SQS queue)
        this.batchSize = options.BatchSize ?? 10; // Size of batch, typically 10.
        // Max time to wait (in seconds) for more messages to arrive (up to batch size) before invoking lambda.
        this.maxBatchWindow = options.MaximumBatchingWindowInSeconds ?? 0;
        this.enabled = options.Enabled ?? true;
        this.lastModified = generator.epoch();
        // this.state = "Enabled"; // Initially "Creating".
        // this.stateTransitionReason = "USER_INITIATED";
        // this.functionResponseTypes = []; // Only ReportBatchItemFailures (i.e. partial fail a batch) supported right now.
    }

    // IDEA: Does this belong here or in the AWS router section?
    getInfo() {
        return { "UUID": this.id, "BatchSize": this.batchSize, "MaximumBatchingWindowInSeconds": this.maxBatchWindow, "EventSourceArn": this.eventSourceArn, "FunctionArn": this.functionArn, "LastModified": this.lastModified, "State": "Enabled", "StateTransitionReason": "USER_INITIATED", "FunctionResponseTypes": [] }
    }
}

// TODO: Fully implement.
class Bucket {
    constructor(name, options) {
        this.name = name;
        this.arn = `arn:aws:s3:::${name}`;
        this.created = generator.epoch();
        this.lastModified = generator.epoch();
        this.tags = {};
    }
}

// TODO: At some point we may support multiple accounts.
const account = new class Account {
    constructor(id=global.account) {
        this.id = id;
        this.roles = {}; // TODO: IAM roles
        this.users = {}; // IAM users
        this.lambdas = {}; // Lambda functions
        this.eventMaps = {}; // Lambda Event Source Mappings
        this.gateways = {}; // API Gateway APIs
        this.tables = {}; // Dynamodb tables
        this.queues = {};  // SQS queues
        this.buckets = {}; // S3 buckets
        this.flows = {}; // TODO: Step Functions
        this.buses = {}; // TODO: Event buses
        this.topics = {}; // TODO: SNS topics
    }

    createRole(name, options, res) {
        if (this.roles[name]) {
            log.error(`Role ${name} already exists.`);
            if (res) res.sendError(409, {
                "Type": "Sender",
                "message": `Role with name ${name} already exists.`,
                "Code": "EntityAlreadyExists"
            }, "iam.CreateRole");
            return;
        }
        const role = new Role(name, options);
        this.roles[name] = role;
        return role;
    }
    getRole(name, operation, res) {
        const role = this.roles[name];
        // Look by arn.
        if (!role) for (const o of Object.values(this.roles)) if (o.arn === name) return o;
        if (!role && res) res.sendError(404, {
            "Type": "Sender",
            "message": `The role with name ${name} cannot be found.`,
            "Code": "NoSuchEntity"
        }, operation);
        return role;
    }

    createLambda(name, options, res) {
        if (this.lambdas[name]) {
            log.error(`Lambda ${name} already exists.`);
            if (res) res.sendError(409, {
                "Type": "User",
                "message": `Function already exists: ${name}`,
                "Code": "ResourceConflictException"
            }, "lambda.CreateFunction");
            return;
        }
    
        const lambda = new Lambda(name, options.Handler, options.Runtime);
        if (options.Role) lambda.role = options.Role;
        if (options.Description) lambda.description = options.Description;
        if (options.MemorySize) lambda.memorySize = options.MemorySize;
        if (options.Code?.S3Bucket) lambda.s3Bucket = options.Code.S3Bucket;
        if (options.Code?.S3Key) lambda.s3Key = options.Code.S3Key;
        this.lambdas[name] = lambda;
        return lambda;
    }
    getLambda(name, operation, res) {
        const lambda = this.lambdas[name];
        // Look by arn.
        if (!lambda) for (const o of Object.values(this.lambdas)) if (o.arn === name) return o;
        if (!lambda && res) res.sendError(404, {
            "Type": "User",
            "message": `Function not found: ${name}`,
            "Code": "ResourceNotFoundException"
        }, operation);
        return lambda;
    }    

    createEventMap(options, res) {
        // TODO: Make sure the lambda and event source exist already.
        // TODO: Check if a mapping for that lambda and event source already exists?
        const eventSourceMapping = new EventSourceMapping(options);
        this.eventMaps[eventSourceMapping.id] = eventSourceMapping;
        return eventSourceMapping;
    }
    getEventMap(id, operation, res) {
        const eventSourceMapping = this.eventMaps[id];
        if (!eventSourceMapping && res) res.sendError(404, {
            "Type": "User",
            "message": `EventSourceMapping not found: ${id}`,
            "Code": "ResourceNotFoundException"
        }, operation);
        return eventSourceMapping;
    }

    createAPIGateway(name, options, res) {
        // Is indexed by id, not name. Check for static tag, otherwise skip since a new id will be generated.
        if (options.tags && options.tags[global.staticApiId]) {
            const id = options.tags[global.staticApiId];
            if (this.gateways[id]) {
                if (res) res.sendError(400, {
                    "__type": "BadRequestException",
                    "message": "Another resource with the same id already exists."
                }, "apigateway.CreateRestApi");
                return;
            }
        }

        const gateway = new APIGateway(name, options.tags);
        if (options.Description) gateway.description = options.description;
        this.gateways[gateway.id] = gateway;
        return gateway;
    }
    getAPIGateway(id, operation, res) {
        const gateway = this.gateways[id];
        if (!gateway && res) res.sendError(404, {
            "__type": "NotFoundException",
            "message": `Invalid API identifier specified`,
        }, operation);
        return gateway;
    }
    
    createTable(name, options, res) {
        if (this.tables[name]) {
            log.error(`Table ${name} already exists.`);
            if (res) res.sendError(400, {
                "__type": "ResourceInUseException",
                "message": "Cannot create preexisting table"
            }, "DynamoDB.CreateTable");
            return;
        }
    
        const table = new Table(name, options);
        // TODO: Move these to constructor and set via options?
        const keySchema = options.KeySchema;
        for (const key of keySchema) {
            if (key.KeyType === "HASH") table.hashKey = key.AttributeName;
            else if (key.KeyType === "RANGE") table.rangeKey = key.AttributeName;
        }
        const attributes = options.AttributeDefinitions, mappedAttrs = {};
        for (const attr of attributes) mappedAttrs[attr.AttributeName] = attr.AttributeType;
        table.attributes = mappedAttrs;
        this.tables[name] = table;
        return table;
    }
    getTable(name, operation, res) {
        let table = this.tables[name];
        // Look by arn.
        if (!table) for (const o of Object.values(this.tables)) if (o.arn === name) return o;
        if (!table && res) res.sendError(404, {
            "__type": "com.amazonaws.dynamodb.v20120810#ResourceNotFoundException",
            "message": `Cannot do operations on a non-existent table ${name}`,
        }, operation);
        return table;
    }

    createQueue(name, options, res) {
        if (this.queues[name]) {
            log.error(`Queue ${name} already exists.`);
            if (res) res.sendError(409, {
                error: {
                    code: 'QueueAlreadyExists',
                    message: 'A queue with this name already exists in this region.'
                }
            }, "AmazonSQS.createQueue");
            return undefined;
        }
        try {
            const queue = new Queue(name, options);
            this.queues[name] = queue;
            return queue;
        } catch (err) {
            log.error(`Error creating queue ${name}: ${err}`);
            if (res) res.sendError(400, {
                error: {
                    code: 'InvalidParameterValue',
                    message: 'Value for parameter is invalid.'
                }
            }, "AmazonSQS.CreateQueue");
        }
    }
    getQueue(name, operation, res) {
        let queue = this.queues[name];
        // Look by arn.
        if (!queue) for (const o of Object.values(this.queue)) if (o.arn === name) return o;
        if (!queue && res) res.sendError(404, {
            message: 'The specified queue does not exist for this wsdl version.',
            code: 'AWS.SimpleQueueService.NonExistentQueue',
        }, operation);
        return queue;
    }

    createBucket(name, options, res) {
        if (this.buckets[name]) {
            log.error(`Bucket ${name} already exists.`);
            if (res) res.sendError(409, {
                "__type": "BucketAlreadyExists",
                "message": "The requested bucket name is not available. The bucket namespace is shared by all users of the system. Please select a different name and try again."
            }, "s3.CreateBucket");
            return;
        }
        const bucket = new Bucket(name, options);
        this.buckets[name] = bucket;
        return bucket;
    }
    getBucket(name, operation, res) {
        let bucket = this.buckets[name];
        if (!bucket && res) res.sendError(404, {
            "__type": "com.amazonaws.s3#NoSuchBucket",
            "message": "The specified bucket does not exist",
        }, operation);
        return bucket;
    }
}


// **************** ROUTES FOR EMULATING AWS APIS ****************

const awsRouter = new Router();
routers["localhost"] = awsRouter;

// As is AWS' practice, they have made every service interface completely different.
awsRouter.post("/", (req, res) => {
    // SQS and DynamoDB use a header.
    if (req.headers["x-amz-target"]) {
        const target = req.headers["x-amz-target"].split(".");
        const service = target[0], action = target[1];

        if (service === "AmazonSQS") {
            const body = JSON.parse(req.body);

            if (action === "CreateQueue") {
                const queue = account.createQueue(body.QueueName, body, res);
                if (queue) res.send(200, { QueueUrl: queue.url }, "AmazonSQS.CreateQueue");

            } else if (action === "DeleteQueue") {
                let name = body.QueueUrl;
                name = name.substring(name.lastIndexOf('/') + 1); // Goes by QueueUrl, not QueueName.
                if (queues[name]) {
                    delete queues[name];
                    res.send(200, {}, "AmazonSQS.DeleteQueue");
                } else {
                    res.sendError(404, {
                        "Type": "Sender",
                        "message": `The specified queue does not exist for this wsdl version.`,
                        "Code": "AWS.SimpleQueueService.NonExistentQueue",
                    }, "AmazonSQS.DeleteQueue");
                }
 
            } else if (action === "GetQueueAttributes") {
                let name = body.QueueUrl;
                name = name.substring(name.lastIndexOf('/') + 1); // Goes by QueueUrl, not QueueName.
                // const name = body.QueueUrl.split('/').pop(); // It queries by QueueUrl, not QueueName.
                const queue = account.getQueue(name, "AmazonSQS.GetQueueAttributes", res); if (!queue) return;
                res.send(200, {
                    "Attributes": {
                        "ApproximateNumberOfMessages": "0", //queue.ApproximateNumberOfMessages,
                        "ApproximateNumberOfMessagesNotVisible": "0", //queue.ApproximateNumberOfMessagesNotVisible,
                        "ApproximateNumberOfMessagesDelayed": "0", //queue.ApproximateNumberOfMessagesDelayed,
                        "CreatedTimestamp": queue.created + "",
                        "DelaySeconds": queue.delaySeconds + "",
                        "LastModifiedTimestamp": queue.lastModified + "",
                        "MaximumMessageSize": queue.maximumMessageSize + "",
                        "MessageRetentionPeriod": queue.messageRetentionPeriod + "",
                        "QueueArn": queue.arn,
                        "ReceiveMessageWaitTimeSeconds": queue.receiveMessageWaitTimeSeconds + "",
                        "VisibilityTimeout": queue.visibilityTimeout + "",
                        "SqsManagedSseEnabled": queue.sqsManagedSseEnabled + ""
                    }
                }, "AmazonSQS.GetQueueAttributes");

            } else if (action === "ListQueueTags") {
                let name = body.QueueUrl;
                name = name.substring(name.lastIndexOf('/') + 1); // Goes by QueueUrl, not QueueName.
                const queue = account.getQueue(name, "AmazonSQS.ListQueueTags", res); if (!queue) return;
                res.send(200, { "Tags": queue.tags }, "AmazonSQS.ListQueueTags");

            // TODO: Add get and put message handlers.
            } else if (action === "SendMessage") {
                let name = body.QueueUrl;
                name = name.substring(name.lastIndexOf('/') + 1); // Goes by QueueUrl, not QueueName.
                const queue = account.getQueue(name, "AmazonSQS.SendMessage", res); if (!queue) return;
                //     QueueUrl: "STRING_VALUE", // required
                //     MessageBody: "STRING_VALUE", // required
                //     DelaySeconds: Number("int"),
                //     MessageAttributes: { // MessageBodyAttributeMap
                //       "<keys>": { // MessageAttributeValue
                //         StringValue: "STRING_VALUE",
                //         BinaryValue: new Uint8Array(), // e.g. Buffer.from("") or new TextEncoder().encode("")
                //         StringListValues: [ // StringList
                //           "STRING_VALUE",
                //         ],
                //         BinaryListValues: [ // BinaryList
                //           new Uint8Array(), // e.g. Buffer.from("") or new TextEncoder().encode("")
                //         ],
                //         DataType: "STRING_VALUE", // required
                //       },
                //     },
                //     MessageSystemAttributes: { // MessageBodySystemAttributeMap
                //       "<keys>": { // MessageSystemAttributeValue
                //         StringValue: "STRING_VALUE",
                //         BinaryValue: new Uint8Array(), // e.g. Buffer.from("") or new TextEncoder().encode("")
                //         StringListValues: [
                //           "STRING_VALUE",
                //         ],
                //         BinaryListValues: [
                //           new Uint8Array(), // e.g. Buffer.from("") or new TextEncoder().encode("")
                //         ],
                //         DataType: "STRING_VALUE", // required
                //       },
                //     },
                //     MessageDeduplicationId: "STRING_VALUE",
                //     MessageGroupId: "STRING_VALUE",
                // }

                res.sendError(400, "TODO: Not yet implemented", "AmazonSQS.SendMessage");
                // { // SendMessageResult
                //   MD5OfMessageBody: "STRING_VALUE",
                //   MD5OfMessageAttributes: "STRING_VALUE",
                //   MD5OfMessageSystemAttributes: "STRING_VALUE",
                //   MessageId: "STRING_VALUE",
                //   SequenceNumber: "STRING_VALUE",
                // };

            } else if (action === "ReceiveMessage") {
                let name = body.QueueUrl;
                name = name.substring(name.lastIndexOf('/') + 1); // Goes by QueueUrl, not QueueName.
                const queue = account.getQueue(name, "AmazonSQS.ReceiveMessage", res); if (!queue) return;
                // QueueUrl: "STRING_VALUE", // required
                // AttributeNames: [ // AttributeNameList
                //     "All" || "Policy" || "VisibilityTimeout" || "MaximumMessageSize" || "MessageRetentionPeriod" || "ApproximateNumberOfMessages" || "ApproximateNumberOfMessagesNotVisible" || "CreatedTimestamp" || "LastModifiedTimestamp" || "QueueArn" || "ApproximateNumberOfMessagesDelayed" || "DelaySeconds" || "ReceiveMessageWaitTimeSeconds" || "RedrivePolicy" || "FifoQueue" || "ContentBasedDeduplication" || "KmsMasterKeyId" || "KmsDataKeyReusePeriodSeconds" || "DeduplicationScope" || "FifoThroughputLimit" || "RedriveAllowPolicy" || "SqsManagedSseEnabled",
                // ],
                // MessageSystemAttributeNames: [ // MessageSystemAttributeList
                //     "All" || "SenderId" || "SentTimestamp" || "ApproximateReceiveCount" || "ApproximateFirstReceiveTimestamp" || "SequenceNumber" || "MessageDeduplicationId" || "MessageGroupId" || "AWSTraceHeader" || "DeadLetterQueueSourceArn",
                // ],
                // MessageAttributeNames: [ // MessageAttributeNameList
                //     "STRING_VALUE",
                // ],
                // MaxNumberOfMessages: Number("int"),
                // VisibilityTimeout: Number("int"),
                // WaitTimeSeconds: Number("int"),
                // ReceiveRequestAttemptId: "STRING_VALUE",

                res.sendError(400, "TODO: Not yet implemented", "AmazonSQS.ReceiveMessage");
                // { // ReceiveMessageResult
                //   Messages: [ // MessageList
                //     { // Message
                //       MessageId: "STRING_VALUE",
                //       ReceiptHandle: "STRING_VALUE",
                //       MD5OfBody: "STRING_VALUE",
                //       Body: "STRING_VALUE",
                //       Attributes: { // MessageSystemAttributeMap
                //         "<keys>": "STRING_VALUE",
                //       },
                //       MD5OfMessageAttributes: "STRING_VALUE",
                //       MessageAttributes: { // MessageBodyAttributeMap
                //         "<keys>": { // MessageAttributeValue
                //           StringValue: "STRING_VALUE",
                //           BinaryValue: new Uint8Array(),
                //           StringListValues: [ // StringList
                //             "STRING_VALUE",
                //           ],
                //           BinaryListValues: [ // BinaryList
                //             new Uint8Array(),
                //           ],
                //           DataType: "STRING_VALUE", // required
                //         },
                //       },
                //     },
                //   ],
                // };

            } else {
                res.sendError(400, `Unknown or unsupported action: ${target}`, "AmazonSQS");
            }

        } else if (service === "DynamoDB_20120810") {
            const body = JSON.parse(req.body);

            if (action === "ListTables") {
                res.send(200, { TableNames: Object.keys(account.tables) }, "DynamoDB.ListTables");

            } else if (action === "Scan") {
                const table = account.getTable(body.TableName, "DynamoDB.Scan", res); if (!table) return;
                // TODO: Support other parameters.
                const response = table.scan();
                res.send(200, response, "DynamoDB.Scan");

            } else if (action === "PutItem") {
                const table = account.getTable(body.TableName, "DynamoDB.PutItem", res); if (!table) return;
                // TODO: Support other parameters.
                const response = table.putItem(body.Item);
                // TODO: Handle returning metrics?
                res.send(200, response, "DynamoDB.PutItem");

            } else if (action === "GetItem") {
                const table = account.getTable(body.TableName, "DynamoDB.GetItem", res); if (!table) return;
                // TODO: Support other parameters.
                const response = table.getItem(body.Key);
                // TODO: Handle returning metrics?
                res.send(200, response, "DynamoDB.GetItem");

            } else if (action === "CreateTable") {
                const table = account.createTable(body.TableName, body, res);
                if (table) res.send(200, {"TableDescription": table.getDescription()}, "DynamoDB.CreateTable");

            } else if (action === "DescribeTable") {
                const table = account.getTable(body.TableName, "DynamoDB.DescribeTable", res); if (!table) return;
                res.send(200, {"Table": table.getDescription()}, "DynamoDB.DescribeTable");

            } else if (action === "DescribeContinuousBackups") {
                const table = account.getTable(body.TableName, "DynamoDB.DescribeContinuousBackups", res); if (!table) return;
                res.send(200, {"ContinuousBackupsDescription": {"ContinuousBackupsStatus": "ENABLED", "PointInTimeRecoveryDescription": {"PointInTimeRecoveryStatus": "DISABLED"}}}, "DynamoDB.DescribeContinuousBackups");

            } else if (action === "DescribeTimeToLive") {
                const table = account.getTable(body.TableName, "DynamoDB.DescribeTimeToLive", res); if (!table) return;
                res.send(200, {"TimeToLiveDescription": {"TimeToLiveStatus": "DISABLED"}}, "DynamoDB.DescribeTimeToLive");

            } else if (action === "ListTagsOfResource") {
                const table = account.getTable(body.ResourceArn, "DynamoDB.ListTagsOfResource", res); if (!table) return;
                res.send(200, {"Tags": []}, "DynamoDB.ListTagsOfResource");

            } else {
                res.sendError(400, `Unknown or unsupported action: ${target}`, "DynamoDB");
            }

        } else {
            res.sendError(400, `Unknown service and action: ${target}`, "unknown");
        }
        return;
    }

    // IAM puts the action in the body, which is encoded like a url.
    if (req.headers["content-type"] === "application/x-www-form-urlencoded") {
        const bodyAsQS = querystring.parse(req.body); // Works for IAM.
        const action = bodyAsQS.Action;

        // IAM operations.
        if (action === "CreateRole") {
            const role = account.createRole(bodyAsQS.RoleName, bodyAsQS, res);
            if (role) res.sendXML(200, "CreateRoleResponse", "https://iam.amazonaws.com/doc/2010-05-08/",
                { CreateRoleResult: role.getInfo() }, "iam.CreateRole");

        } else if (action === "GetRole") {
            const role = account.getRole(bodyAsQS.RoleName, "iam.GetRole", res); if (!role) return;
            res.sendXML(200, "GetRoleResponse", "https://iam.amazonaws.com/doc/2010-05-08/",
                { GetRoleResult: role.getInfo() }, "iam.GetRole");

        } else if (action === "ListRolePolicies") {
            // const role = account.getRole(bodyAsQS.RoleName, "iam.ListRolePolicies", res); if (!role) return;
            // TODO: Get policies.
            res.sendXML(200, "ListRolePoliciesResponse", "https://iam.amazonaws.com/doc/2010-05-08/",
                { ListRolePoliciesResult: { PolicyNames: undefined, IsTruncated: false, } }, "iam.ListRolePolicies");

        } else if (action === "ListAttachedRolePolicies") {
            // const role = account.getRole(bodyAsQS.RoleName, "iam.ListAttachedRolePolicies", res); if (!role) return;
            // TODO: Get attached policies.
            res.sendXML(200, "ListAttachedRolePoliciesResponse", "https://iam.amazonaws.com/doc/2010-05-08/", {
                ListAttachedRolePoliciesResult: { AttachedPolicies: undefined, IsTruncated: false, }
            }, "iam.ListAttachedRolePolicies");

        } else if (action === "GetUser") { // Called if skip_requesting_account_id not set.
            // Request: { method: 'POST', path: '/', body: 'Action=GetUser&Version=2010-05-08' }
            res.sendError(400, `${action} not implemented.`, "iam.GetUser");

        } else if (action === "ListRoles") {
            //   Request: { method: 'POST', path: '/', body: 'Action=ListRoles&MaxItems=1&Version=2010-05-08' }
            res.sendError(400, `${action} not implemented.`, "iam.ListRoles");

        } else if (action === "GetCallerIdentity") { // Called if skip_requesting_account_id not set.
            // Actually an STS call, not IAM.
            res.sendXML(200, "GetCallerIdentityResponse", "https://iam.amazonaws.com/doc/2010-05-08/", {
                GetCallerIdentityResult: {
                    Arn: `arn:aws:iam::${global.account}:user/FakeUser`,
                    Account: global.account, UserId: "AIDAJDPLRKLG7UEXAMPLE"
                }
            }, "sts.GetCallerIdentity")

        } else {
            res.sendError(400, "Unknown IAM action.", "iam ??????");
        }
        return;
    }

    // TODO: What now? We could try to parse the body as json and look for an Action key?
    res.sendError(400, "No action found.", "unknown");
});

// ************************** Lambda **************************
// https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/client/lambda/

function getLambdaInfo(lambda) {
    return {
        FunctionName: lambda.name, FunctionArn: lambda.arn, Runtime: lambda.runtime, Role: lambda.role, Handler: lambda.handler, CodeSize: 0, Description: lambda.description, "Timeout": lambda.timeout, "MemorySize": lambda.memorySize, "LastModified": lambda.lastModified, "CodeSha256": "na", "Version": lambda.version, "TracingConfig": {"Mode": "PassThrough"}, "RevisionId": "df320854-5db9-4582-b3c0-a5b209562728", State: lambda.state, StateReason: Lambda.stateReason[lambda.stateReasonCode], StateReasonCode: lambda.stateReasonCode, "PackageType": "Zip", "Architectures": ["x86_64"], "EphemeralStorage": {"Size": 512}, "SnapStart": {"ApplyOn": "None", "OptimizationStatus": "Off"}, "RuntimeVersionConfig": {"RuntimeVersionArn": `arn:aws:lambda:${global.region}::runtime:00000`}, "LoggingConfig": { "LogFormat": "Text", "LogGroup": lambda.logGroup }
    };
}

awsRouter.post("/2015-03-31/functions", (req, res) => {
    const body = JSON.parse(req.body);
    const lambda = account.createLambda(body.FunctionName, body, res);
    if (lambda) res.send(201, getLambdaInfo(lambda), "lambda.CreateFunction");
});

awsRouter.get("/2015-03-31/functions/{name}", (req, res) => {
    const lambda = account.getLambda(req.params.name, "lambda.GetFunction", res); if (!lambda) return;
    res.send(200, JSON.stringify({
        Configuration: getLambdaInfo(lambda),
        Code: { "Location": `Code location: ${lambda.s3Key}`, "RepositoryType": "S3" },
        Tags: lambda.tags,
        // Concurrency
    }), "lambda.GetFunction");
});

awsRouter.get("/2015-03-31/functions/{name}/versions", (req, res) => {
    const lambda = account.getLambda(req.params.name, "lambda.ListVersionsByFunction", res); if (!lambda) return;
    res.send(200, JSON.stringify({ "Versions": [getLambdaInfo(lambda)] }), "lambda.ListVersionsByFunction");
});

awsRouter.get("/2015-03-31/functions/{name}/policy", (req, res) => {
    const lambda = account.getLambda(req.params.name, "lambda.GetPolicy", res); if (!lambda) return;
    res.send(200, JSON.stringify({Policy: JSON.stringify({
        "Version": "2012-10-17", "Id": "default", "Statement": [{"Sid": "AllowExecutionFromAPIGateway", "Effect": "Allow", "Action": "lambda:InvokeFunction", "Resource": lambda.arn, "Principal": {"Service": "apigateway.amazonaws.com"}, "Condition": {"ArnLike": {"AWS:SourceArn": `arn:aws:execute-api:${global.region}::example/*/*`}}}]
    }), "RevisionId": "1057847c-edc4-4342-bdfa-fccd17cb341c"}), "lambda.GetPolicy");
});

awsRouter.post("/2015-03-31/functions/{name}/policy", (req, res) => {
    const lambda = account.getLambda(req.params.name, "lambda.AddPermission", res); if (!lambda) return;
//     body: '{"Action":"lambda:InvokeFunction","Principal":"apigateway.amazonaws.com","SourceArn":"arn:aws:execute-api:us-east-1::example/*/*","StatementId":"AllowExecutionFromAPIGateway"}'
    res.send(200, JSON.stringify({Statement: JSON.stringify({
        "Sid": "AllowExecutionFromAPIGateway", "Effect": "Allow", "Action": "lambda:InvokeFunction", "Resource": lambda.arn, "Principal": {"Service": "apigateway.amazonaws.com"}, "Condition": {"ArnLike": {"AWS:SourceArn": `arn:aws:execute-api:${global.region}::example/*/*`}}
    }) }), "lambda.AddPermission");
});

awsRouter.get("/2020-06-30/functions/{name}/code-signing-config", (req, res) => {
    res.send(200, {}, "lambda.GetFunctionCodeSigningConfig");
});

awsRouter.post("/2015-03-31/functions/{name}/invocations", async (req, res) => {
    const lambda = account.getLambda(req.params.name, "lambda.Invoke", res); if (!lambda) return;

    // Determine how they would like the lambda invoked:
    const invocationType = req.headers["x-amz-invocation-type"] ?? "RequestResponse";
    if (invocationType === "DryRun") { // Validate parameter values & verify the user/role has permission to invoke. 
        res.send(204, "", "lambda.Invoke");
    } else if (invocationType === "Event") {
        // Invoke the lambda asynchronously. TODO: Send events that fail multiple times to the function's
        // dead-letter queue (if one is configured). The API response only includes a status code.
        lambda.invoke(req.body, res.requestId) // FIXME: req.body is still a string here. Is that desired?
            .catch(e => log.error(`Async lambda event invocation error: ${e}`));
        res.send(202, "", "lambda.Invoke");
    } else { // RequestResponse
        try {
            response = await lambda.invoke(req.body, res.requestId); // FIXME: req.body is still a string here. Is that desired?
            res.send(response.StatusCode, JSON.stringify({
                "event":req.body, "context":{"callbackWaitsForEmptyEventLoop":true, "functionVersion": lambda.version,"functionName": lambda.name, "memoryLimitInMB": lambda.memorySize, "logGroupName": lambda.logGroup, "logStreamName": lambda.logStream, "invokedFunctionArn": lambda.arn, "awsRequestId":res.requestId}
            }), "lambda.Invoke");
        } catch (e) {
            // { StatusCode: code, FunctionError: error.name ?? "Unhandled", ExecutedVersion: '$LATEST' }
            res.sendError(500, e.message, "lambda.Invoke");
        }
    }
});


// ************************** Event Source Mapping (Lambda) **************************
// TODO: Add support for deleteEventSourceMapping, listEventSourceMappings, updateEventSourceMapping

awsRouter.post("/2015-03-31/event-source-mappings", (req, res) => {
    const eventSourceMapping = account.createEventMap(JSON.parse(req.body), res);
    res.send(202, eventSourceMapping.getInfo(), "lambda.CreateEventSourceMapping");
});

awsRouter.get("/2015-03-31/event-source-mappings/{id}", (req, res) => {
    const eventSourceMapping = account.getEventMap(req.params.id, "lambda.GetEventSourceMapping", res);
    if (!eventSourceMapping) return;
    res.send(200, eventSourceMapping.getInfo(), "lambda.GetEventSourceMapping");
});


// ************************** API Gateway v1 **************************

function getAPIGatewayInfo(gateway) {
    return `{"id": "${gateway.id}", "name": "${gateway.name}", "description": "${gateway.description}", "createdDate": ${gateway.createDate}, "apiKeySource": "HEADER", "endpointConfiguration": {"types": ["EDGE"]}, "tags": ${JSON.stringify(gateway.tags)}, "disableExecuteApiEndpoint": false, "rootResourceId": "${gateway.root.id}"}`
}

awsRouter.post("/restapis", (req, res) => {
    const body = JSON.parse(req.body);
    const gateway = account.createAPIGateway(body.name, body, res);
    if (gateway) res.send(201, getAPIGatewayInfo(gateway), "apigateway.CreateRestApi");
});

awsRouter.get("/restapis/{id}", (req, res) => {
    const gateway = account.getAPIGateway(req.params.id, "apigateway.GetRestApi", res); if (!gateway) return;
    res.send(200, getAPIGatewayInfo(gateway), "apigateway.GetRestApi");
});

awsRouter.put("/restapis/{id}", (req, res) => {
    const gateway = account.getAPIGateway(req.params.id, "apigateway.GetRestApi", res); if (!gateway) return;
    // query: { mode: 'overwrite' },
    // body: '{"openapi":"3.0.0","paths":{"/hello":{"x-amazon-apigateway-any-method":{"x-amazon-apigateway-integration":{"httpMethod":"POST","type":"AWS_PROXY","uri":"arn:aws:apigateway:us-east-1:lambda:path/2015-03-31/functions/arn:aws:lambda:us-east-1:000000000000:function:hello/invocations"}}}}}'
    // content-type: application/octet-stream?

    // TODO: Add the routes to the gateway. Each part of the path is a resource,
    // so "/test/{name}/hello" would be 4 nested resources (including the default "/" one).
    res.send(200, getAPIGatewayInfo(gateway), "apigateway.PutRestApi");
});

awsRouter.patch("/restapis/{id}", (req, res) => {
    const gateway = account.getAPIGateway(req.params.id, "apigateway.GetRestApi", res); if (!gateway) return;
    // body: '{"patchOperations":[{"op":"replace","path":"/description","value":"API for Example Lambda Function"}]}'
    // TODO: Update the gateway per the passed parameters?
    res.send(200, getAPIGatewayInfo(gateway), "apigateway.UpdateRestApi");
});

awsRouter.get("/restapis/{id}/resources", (req, res) => {
    const gateway = account.getAPIGateway(req.params.id, "apigateway.GetRestApi", res); if (!gateway) return;
    // TODO: Don't hardcode these return values.
    res.send(200, `{"item": [{"id": "1kqoyoqwwz", "path": "/"}, {"id": "21vhnwztri", "parentId": "1kqoyoqwwz", "pathPart": "hello", "path": "/hello", "resourceMethods": {"ANY": {"httpMethod": "ANY", "authorizationType": "NONE", "apiKeyRequired": false, "methodResponses": {}, "methodIntegration": {"type": "AWS_PROXY", "httpMethod": "POST", "uri": "arn:aws:apigateway:${global.region}:lambda:path/2015-03-31/functions/arn:aws:lambda:${global.region}:${global.account}:function:hello/invocations", "passthroughBehavior": "WHEN_NO_MATCH", "timeoutInMillis": 29000, "cacheNamespace": "21vhnwztri", "cacheKeyParameters": []}}}}]}`, "apigateway.GetResources");
});

awsRouter.post("/restapis/{id}/deployments", (req, res) => {
    // body: '{"description":"","stageDescription":"","stageName":"dev","variables":{}}'
    // TODO: Don't hardcode these return values.
    res.send(201, `{"id": "02etgoboha", "description": "", "createdDate": 1715727569.0}`, "apigateway.CreateDeployment");
});

awsRouter.get("/restapis/{id}/deployments/{id}", (req, res) => {
    // TODO: Don't hardcode these return values.
    res.send(200, `{"id": "02etgoboha", "description": "", "createdDate": 1715727569.0}`, "apigateway.GetDeployment");
});


// ************************** S3 **************************
awsRouter.head("/{bucket}", (req, res) => {
    res.set("x-amz-id-2", "none"); // This is passed back for all s3. It is encrypted support data.
    res.send((account.getBucket(req.params.bucket) ? 200 : 404), "", "s3.GetHeadBucket");
    // On a 200 it passes these header back as well:
    // 'x-amz-bucket-region': 'us-east-1',
});

awsRouter.put("/{bucket}", (req, res) => {
    res.set("x-amz-id-2", "none"); // This is passed back for all s3. It is encrypted support data.
    // TODO: Convert the header parameters to a map of some sort?    'x-amz-acl': 'private',
    const bucket = account.createBucket(req.params.bucket, req.headers, res);
    if (bucket) res.send(200, "", "s3.CreateBucket");
    // Passes cors and these back too: location: '/my-tf-test-bucket', vary: 'Origin',
});

// TODO: Implement these to at least create an empty bucket.
// GetBucketPolicy
// GetBucketAcl
// GetBucketCors
// GetBucketWebsite
// GetBucketVersioning
// GetBucketAccelerateConfiguration
// GetBucketRequestPayment
// GetBucketLogging
// GetBucketLifecycleConfiguration
// GetBucketReplication
// GetBucketEncryption
// GetObjectLockConfiguration
// GetBucketTagging


// ************************** Console UI **************************

{
    const router = new Router();
    routers["console.localhost"] = router;

    router.get("/", (req, res) => {
        res.type("text/html").send(200, `<!DOCTYPE html><html lang="en"><meta charset="utf-8"/><title>Console</title><link rel="icon" type="image/svg+xml" href="favicon.svg"><style>* { margin:0; padding:0; box-sizing:border-box; font-family:arial, helvetica, sans-serif; }</style></style><body><textarea style="width:100%; height:100vh;" id="list">Loading...</textarea><script>const baseURL = "http://console.localhost:3000/"; async function restCall(method, query, body) {
    console.log(method, baseURL + query, body);
    const options = { method };
    if (body) { // body should be json serializable.
        options.headers = {"Content-Type": "application/json"};
        options.body = JSON.stringify(body);
    }
    const response = await fetch(baseURL + query, options);
    if (!response.ok) throw new Error(response.status + " " + response.statusText);
    const data = await response.json();
    console.log(data);
    return data;
} async function init() { const result = await restCall("GET", "info"); document.querySelector("#list").replaceChildren(JSON.stringify(result, null, 2)); } init();</script></body></html>`, "console");
    });

    router.get("/favicon.svg", (req, res) => {
        res.type("image/svg+xml").send(200, `<svg color="#000000" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="m7 14-2 7c3.91 2.26 10.2 2.18 14 0l-2-7z" style="fill:#f3ed95"/><path d="m7 8c-2.99 0.556-5 1.59-5 2.78 0 1.78 4.48 3.22 10 3.22 5.52 0 10-1.44 10-3.22 0-1.19-2.01-2.23-5-2.78" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" style="fill:#ced4d2"/><path d="m7.32 8.84c-0.449-0.625-0.354-1.03-0.164-1.83 0.527-2.22 2.46-4.01 4.84-4.01s4.31 1.79 4.84 4.01c0.19 0.801 0.285 1.2-0.164 1.83-1.22 1.71-8.37 1.38-9.36 0z" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" style="fill:#86c8d7"/><path d="m12 16.3-2.32 0.735 2.34 0.78 2.4-0.738zm-2.91 1.19v3.63l2.57 0.871v-3.71zm5.87 0.0519-2.57 0.79v3.71l2.57-0.871z" style="fill:#f8991d"/></svg>`, "console");
    });

    router.get("/info", (req, res) => {
        const response = {};
        // TODO: Send users back with roles?
        response.identities = [];
        for (const role of Object.values(account.roles)) response.identities.push({
            name: role.name,
            id: role.id,
            type: "role"
        });
        response.lambdas = [];
        for (const lambda of Object.values(account.lambdas)) response.lambdas.push({
            name: lambda.name,
            runtime: lambda.runtime
        });
        response.apis = [];
        for (const api of Object.values(account.gateways)) response.apis.push({
            name: api.name,
            id: api.id
        });
        response.tables = [];
        for (const table of Object.values(account.tables)) response.tables.push({
            name: table.name,
            numItems: table.itemCount
        });
        response.queues = [];
        for (const queue of Object.values(account.queues)) response.queues.push({
            name: queue.name,
            numItems: queue.messages.length
        });
        response.buckets = [];
        for (const bucket of Object.values(account.buckets)) response.buckets.push({
            name: bucket.name,
            // TODO: Add more details?
        });
        // TODO: Flows, Buses, Topics
       res.send(200, response, "console.info");
    })
}


// ************************** Initialization **************************

function fileExists(filePath) {
    return fs.existsSync(filePath) && fs.statSync(filePath).isFile();
}
function directoryExists(directoryPath) {
    return fs.existsSync(directoryPath) && fs.statSync(directoryPath).isDirectory();
}

// Determine how to find out if python or python3 is the correct command for this computer.
function getPythonCommand() {
    try { // Python3 is typical for many linux systems.
        const python3VersionOutput = execSync("python3 --version", { stdio: "pipe" }).toString();
        if (python3VersionOutput.includes("Python")) return "python3";
    } catch (error) { }
    try {
        const pythonVersionOutput = execSync("python --version", { stdio: "pipe" }).toString();
        if (pythonVersionOutput.includes("Python")) return "python";
    } catch (error) { }
    return "PythonNotFound";
}

async function init() {
    const configFile = process.cwd() + "/grove.json";
    if (fileExists(configFile)) {
        try {
            // Remove any lines that start with a comment before parsing. JSON's lack of comments are annoying.
            const config = JSON.parse( (await fsp.readFile(configFile, "utf8")).replace(/^\s*\/\/.*$/gm, "") );
            for (const key in global) if (config[key]) {
                global[key] = config[key];
                log.info(`Set global.${key} to ${config[key]}`);
            }
            // Make sure we always have something for node and python.
            if (!global.launch) {
                global.launch = { "nodejs": "node", "python": getPythonCommand() };
            } else {
                if (!global.launch.nodejs) global.launch.nodejs = "node";
                if (!global.launch.python) global.launch.python = getPythonCommand();
            }

            // lambdas: {
            //     "hello": { src: "./lambda", handler: "hello.handler", vars: { "NAME": "Larry" }},
            // },
            if (config.lambdas) {
                for (let [name, props] of Object.entries(config.lambdas)) {
                    if (account.lambdas[name]) { log.error(`Lambda ${name} already exists.`); continue; }
                    // Assuming they are hot reloading.
                    if (!props.src || props.src === ".") props.src = process.cwd();
                    else if (!directoryExists(props.src)) {
                        log.error(`Lambda ${name} src directory doesn't exist.`); continue;
                    }
                    if (!props.handler) props.handler = name + ".handler";
                    if (!props.runtime) {
                        const files = fs.readdirSync(props.src);
                        const match = files.find(file => file.startsWith(props.handler.split(".")[0] + '.'));
                        if (match) {
                            const ext = match.substring(match.lastIndexOf("."));
                            if (ext === ".py") props.runtime = "python3.12";
                            else if (ext === ".js" || ext === ".mjs" || ext === ".cjs") props.runtime = "nodejs20.x";
                        }
                        if (!props.runtime) { log.error(`Lambda ${name} must have a runtime.`); continue; }
                    } else {
                        // Check the lambda file exists.
                        const filename = props.handler.substring(props.handler.indexOf(".") + 1);
                        let found = false;
                        if (props.runtime.startsWith("nodejs")) {
                            found = (fileExists(filename + ".js")
                                || fileExists(filename + ".mjs")
                                || fileExists(filename + ".cjs"));
                        } else if (props.runtime.startsWith("python")) {
                            found = (fileExists(filename + ".py"));
                        }
                        if (!found) { log.error(`Lambda ${name} file does not exist.`); continue; }
                    }

                    // FIXME: Use account.createLambda?
                    const lambda = new Lambda(name, props.handler, props.runtime);
                    lambda.s3Key = props.src;
                    lambda.environment.variables = props.vars;
                    // TODO: Add other properties.
                    account.lambdas[name] = lambda;
                    log.info(`Lambda ${name} created via config.`);
                }
            }

            //     apis: {
            //         "example": { // id/name
            //             "dev": { // stage
            //                 // Can use ANY to match any method.
            //                 // Value for method can be a lambda name or url? If url, then need more attributes (ex. method).
            //                 "/hello": { "GET": "hello" }
            //             }
            //         }
            //     }
            if (config.apis) {
                for (let [id, props] of Object.entries(config.apis)) { // Gateways
                    if (account.gateways[id]) { log.error(`API Gateway ${id} already exists.`); continue; }
                    // FIXME: Use account.createAPIGateway?
                    const gateway = new APIGateway(id, {[global.staticApiId]: id});
                    for (let [stage, paths] of Object.entries(props)) { // Stages
                        for (let [path, methods] of Object.entries(paths)) { // Paths
                            for (let [method, target] of Object.entries(methods)) { // Methods
                                method = method.toUpperCase();
                                if (!APIGateway.supportedMethods.includes(method)) {
                                    log.error(`Invalid http method ${method} for ${id}.${stage}.${path}.`); continue;
                                }
                                if (!account.getLambda(target)) {
                                    log.error(`Lambda ${target} not a valid target for ${id}.${stage}.${path}.`); continue;
                                }
                                gateway.addRoute(stage, path, method, target);
                            }
                        }
                    }
                    account.gateways[id] = gateway;
                    log.info(`Gateway ${id} created via config at http://${gateway.domain}:${global.port}/`);
                }
            }

            // "tables": {
            //     "GameScores": { // Table name
            //         "hashKey": "UserId", // a.k.a. partition key. Values in this key must be unique.
            //         "rangeKey": "GameTitle", // a.k.a. sort key. This key is optional.
            //         "attributes": { "UserId": "S", "GameTitle": "S" }
            //         "globalIndexes": { // Up to 20 entries
            //             "GameTitleIndex": { // Index name, must be unique in the table (including local).
            //                 "hashKey": "GameTitle", "rangeKey": "TopScore", // range is optional
            //                 // All secondary index attr names combined can't exceed 100. Duplicates are considered distinct.
            //                 "projection": { "type": "ALL|INCLUDE|KEYS_ONLY", "keys": ["attr name, excluding hash and ranges ones"] },
            //             },
            //         },
            //         "localIndexes": { // Up to 5 entries.
           //             "AnotherIndex": { // Index name, must be unique in the table (including global).
            //                 "rangeKey": "Something", // hashkey must be the same as table for a local, so not included.
            //                 // All secondary index attr names combined can't exceed 100. Duplicates are considered distinct.
            //                 "projection": { "type": "ALL|INCLUDE|KEYS_ONLY", "keys": ["attr name, excluding hash and ranges ones"] },
            //             },
            //         }
            //     }
            // }
            if (config.tables) {
                for (let [name, props] of Object.entries(config.tables)) {
                    if (account.tables[name]) { log.error(`Table ${name} already exists.`); continue; }
                    // FIXME: Use account.createTable?
                    const table = new Table(name);
                    table.hashKey = props.hashKey;
                    if (props.attributes[props.hashKey]) {
                        table.hashKeyType = props.attributes[props.hashKey];
                    } else {
                        log.error(`Table ${name} missing ${props.hashKey} from attribute list.`); continue;
                    }
                    if (props.rangeKey) {
                        table.rangeKey = props.rangeKey;
                        if (props.attributes[props.rangeKey]) {
                            table.rangeKeyType = props.attributes[props.rangeKey];
                        } else {
                            log.error(`Table ${name} missing ${props.rangeKey} from attribute list.`); continue;
                        }
                    }
                    table.attributes = props.attributes;
                    account.tables[name] = table;
                    log.info(`Table ${name} created via config.`);
                }
            }
        } catch (err) {
            log.error(err);
            return;
        }
    }

    const server = http.createServer(async (req, res) => {
        let host = req.headers.host;
        const portSep = host.indexOf(":")
        if (portSep > -1) host = host.substring(0, portSep);
        // Pick a router based on the host name (localhost is AWS SDK, <id>.execute-api.<region>.localhost is API Gateway, etc.)
        const router = routers[host];
        if (router) router.route(req, res);
        else {
            res.statusCode = 404;
            const message = `No route for [${req.method}] ${req.url}`;
            res.end(message);
            log.warn(message);
        }
    });
    server.listen(global.port, () => { log.info(`Server running at http://localhost:${global.port}/`); });    
}
init();